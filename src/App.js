import logo from './logo.svg';
import './App.css';
import Button from 'react-bootstrap/Button';
import Carding from './Components/Carding';

class App extends React.Component {
  render() {
    const [Person, setPerson] = useState({
      fullName : "George",
      bio :"GoMyCode",
      imgSrc:"",
      profession:"Informatics"
    });
    var BooleanShow = false

    const toggleShowTrue = () => {
      BooleanShow = true;
    }
    const toggleShowFalse = () => {
      BooleanShow = false;
    }
    return (
      <div className="App">
        <Button variant="primary" onClick={()=> toggleShowTrue}>Click to show</Button>
        <Button variant="primary">Click to hide</Button>
        if(BooleanShow){
          <Carding/>
        }
      </div>
    )
  }
}

export default App;
